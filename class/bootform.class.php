<?php
    class BootForm{

        public $version = "4.3.1";
        public $formControl = "form-control";
        public $size = "";
        public $selectStateFalseName = "Inactivo";
        public $selectStateTrueName = "Activo";
        public $selectStateFalseValue = 0;
        public $selectStateTrueValue = 1;

        function __construct(
            $size = "",
            $selectStateFalse = null,
            $selectStateTrue = null,
            $selectStateFalseValue = null,
            $selectStateTrueValue = null
        ){
            if(in_array(trim($size), ["lg", "sm"])){
                switch(trim($size)){
                    case "sm": $this->size = "form-control-sm"; break;
                    case "lg": $this->size = "form-control-lg"; break;
                }
            }
            $this->setSelectStateNames($selectStateFalse, $selectStateTrue);
            $this->setSelectStateValues($selectStateFalseValue, $selectStateTrueValue);
        }

        public function setSelectStateNames($selectStateFalse = null, $selectStateTrue = null){
            if(!is_null($selectStateFalse)){
                $this->selectStateFalseValue = $selectStateFalse;
            }
            if(!is_null($selectStateTrue)){
                $this->selectStateTrueValue = $selectStateTrue;
            }
        }

        public function setSelectStateValues($selectStateFalseValue = null, $selectStateTrueValue = null){
            if(!is_null($selectStateFalseValue)){
                $this->selectStateFalseValue = $selectStateFalseValue;
            }
            if(!is_null($selectStateTrueValue)){
                $this->selectStateTrueValue = $selectStateTrueValue;
            }
        }

        public function getSelectStateValues(){
            return [false => $this->selectStateFalseValue, true => $this->selectStateTrueValue];
        }

        static private function arrayToStringDatas(array $datas = []){
            $dataString = "";
            foreach($datas as $key => $value) $dataString .= " data-" . $key . "='" . $value . "'";
            return $dataString;     
        }

        /*********************************************/
        /*** COMPONENTES FORMULARIO ESTANDARIZADOS ***/
        /*********************************************/
        /**
         * Input
         *
         * @param string $type
         * @param string $label         nombre de la etiqueta
         * @param null $name            "name" que corresponde al input a vincular
         * @param string $value
         * @param bool $active
         * @param string $customClass
         * @param string $placeholder
         *
         * @return string
         */
        public function inputText(
            $type = "text",
            $label = "",
            $name = null,
            $value = "",
            $active = true,
            $customClass = "",
            $placeholder = ""
        ){
            if($type == "hidden"){
                return "<input type='" . $type . "' name='" . $name . "' value='" . $value . "'>";
            }
            $html = "<div class='form-group'>";
            $html .= $this->getLabel($label, $name);
            $html .= "<input type='" . $type . "' class='" . $this->formControl . " " . $this->size . " " . $customClass . "' name='" . $name . "' value='" . $value . "' placeholder='" . $placeholder . "' " . (!$active ? "disabled" : "") . ">";
            $html .= "</div>";

            return $html;
        }

        /**
         * Label
         *
         * @param null $label
         * @param string $for
         *
         * @return string
         */
        private function getLabel($label = null, $for = ""){
            $html = "";
            if(!is_null($label)){
                $html = "<label" . ($for !== "" ? " for='" . $for . "'" : "") . ">" . $label . "</label>";
            }

            return $html;
        }

        /**
         * Textarea
         *
         * @param string $label
         * @param null $name
         * @param string $value
         * @param bool $active
         * @param string $customClass
         * @param string $placeholder
         *
         * @return string
         */
        public function textArea($label = "", $name = null, $value = "", $active = true, $customClass = "", $placeholder = ""){
            $html = "<div class='form-group'>";
            $html .= $this->getLabel($label, $name);
            if($active){
                $html .= "<textarea class='" . $this->formControl . " " . $this->size . " " . $customClass . "' name='" . $name . "' placeholder='" . $placeholder . "' " . (!$active ? "disabled" : "") . ">" . $value . "</textarea>";
            }else{
                $html .= "<div class='card'>";
                $html .= "<div class='card-body'>";
                $html .= $value;
                $html .= "</div>";
                $html .= "</div>";
            }
            $html .= "</div>";

            return $html;
        }

        /**
         * Checkbox
         *
         * @param $label
         * @param null $name
         * @param bool $state
         * @param bool $active
         * @param string $customClass
         *
         * @return string
         */
        public function inputCheckbox($label = "", $name = null, $state = false, $active = true, $customClass = ""){
            $html = "<div class='form-check'>";
            $html .= "<input class='form-check-input " . $customClass . "' type='checkbox' name='" . $name . "' " . ($state ? "checked selected" : "") . " " . (!$active ? "disabled" : "") . ">";
            $html .= "<label class='form-check-label' for='" . $name . "'>" . $label . "</label>";
            $html .= "</div>";

            return $html;
        }

        /**
         * Radio
         *
         * @param $label
         * @param null $name
         * @param bool $state
         * @param bool $active
         * @param string $customClass
         *
         * @return string
         */
        public function inputRadio($label = "", $name = null, $state = false, $active = true, $customClass = ""){
            $html = "<div class='radio'>";
            $html .= "<label>";
            $html .= "<input class='" . $customClass . "' type='radio' name='" . $name . "' " . ($state ? "checked" : "") . " " . ($state == '1' ? "selected" : "") . " " . (!$active ? "disabled" : "") . "> " . $label;
            $html .= "</label>";
            $html .= "</div>";

            return $html;
        }
    
        /**
         * Select
         *
         * @param string $label
         * @param null $name
         * @param array $option
         * @param bool $state
         * @param bool $active
         * @param string $customClass
         * @param array $datas
         *
         * @return string
         */
        public function selectOptions(
            string $label = "", 
            $name = null, 
            array $option = [], 
            $state = false, 
            $active = true,
            string $customClass = "", 
            array $datas = []
        ){
            $html = "<div class='form-group'>";
            $html .= $this->getLabel($label, $name);
            $html .= "<select 
                            name='" . $name . "' 
                            class='" . $this->formControl . " " . $this->size . " " . $customClass . "'" . (!$active ? " disabled" : "") . "
                            " . self::arrayToStringDatas($datas) . ">";
            foreach($option as $key => $value){
                if($active || $key == $state){
                    $html .= "<option value='" . $key . "'" . ($state == $key ? " selected" : "") . ">" . $value . "</option>";    
                }
            }
            $html .= "</select>";
            $html .= "</div>";
        
            return $html;
        }

        /**
         * Select - State
         *
         * @param string $label
         * @param null $name
         * @param bool $state
         * @param bool $active
         * @param string $customClass
         *
         * @return string
         */
        public function selectState($label = "", $name = null, $state = false, $active = true, $customClass = ""){
            $html = "<div class='form-group'>";
            $html .= $this->getLabel($label, $name);
            $html .= "<select name='" . $name . "' class='" . $this->formControl . " " . $this->size . " " . $this->size . " " . $customClass . "'" . (!$active ? " disabled" : "") . ">";
            $html .= "<option value='" . $this->selectStateFalseValue . "'" . (!$state ? " selected" : "") . ">" . $this->selectStateFalseName . "</option>";
            $html .= "<option value='" . $this->selectStateTrueValue . "'" . ($state ? " selected" : "") . ">" . $this->selectStateTrueName . "</option>";
            $html .= "</select>";
            $html .= "</div>";

            return $html;
        }

        /**
         * Botones
         *
         * @param string $type
         * @param string $label
         * @param string $size
         * @param string $btnClass
         * @param bool $active
         * @param string $customClass
         *
         * @return string
         */
        public function button($type = "button", $label = "", $size = "", $btnClass = "default", $active = true, $customClass = ""){
            if($size !== ""){
                $size = "btn-" . $size;
            }
            $html = "<button type='" . $type . "' class='btn btn-" . $btnClass . " " . $size . " " . $customClass . "' role='button' " . (!$active ? " disabled" : "") . ">";
            $html .= $label;
            $html .= "</button >";

            return $html;
        }

    }