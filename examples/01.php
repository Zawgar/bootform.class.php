<?php
    include("includes/head.php");
    include("../class/bootform.class.php");
    $bootform = new BootForm();
    
    // input text
    echo $bootform->inputText("text", "Texto", "nombre", "", true, "", "ingrese su nombre");
    echo $bootform->inputText("text", "Texto", "nombre", "", false, "", "ingrese su nombre");
    
    // select
    $arrSelect = [
        1 => "Uno", 
        2 => "Dos", 
        3 => "Tres", 
        4 => "Cuatro"
    ];
    echo $bootform->selectOptions("Seleccionar opción", "opciones", $arrSelect, "3");
    echo $bootform->selectOptions("Seleccionar opción", "opciones", $arrSelect, "3", false);
    echo "<hr>";
    
    // select state
    echo $bootform->selectState("Disponible", "disponible", true);
    echo $bootform->selectState("Disponible", "disponible", true, false);
    echo "<hr>";
    
    // checkbox
    echo $bootform->inputCheckbox("Arriba", "opcion_01", true);
    echo $bootform->inputCheckbox("Abajo", "opcion_02", false, false);
    echo $bootform->inputCheckbox("Adentro", "opcion_03", false);
    echo "<hr>";

    // radio
    echo $bootform->inputRadio("Primero", "radio_option", true);
    echo $bootform->inputRadio("Segundo", "radio_option", false, false);
    echo $bootform->inputRadio("Tercero", "radio_option", false);
    echo "<hr>";
    
    // textarea
    echo $bootform->textArea("Descripción", "decripcion", "", true, "", "ingrese alguna descripción");
    echo "<hr>";
    
    // botones
    echo $bootform->button("button", "Boton", "", "outline-primary") . " ";
    echo $bootform->button("button", "Boton", "", "outline-secondary") . " ";
    echo $bootform->button("button", "Boton", "", "outline-success") . " ";
    echo $bootform->button("button", "Boton", "", "outline-info") . " ";
    echo $bootform->button("button", "Boton", "", "outline-warning") . " ";
    echo $bootform->button("button", "Boton", "", "outline-danger") . " ";
    echo $bootform->button("button", "Boton", "", "outline-dark") . " ";
    echo $bootform->button("button", "Boton", "", "outline-light") . " ";
    echo "<br>";
    echo $bootform->button("button", "Boton", "", "outline-primary", false) . " ";
    echo $bootform->button("button", "Boton", "", "outline-secondary", false) . " ";
    echo $bootform->button("button", "Boton", "", "outline-success", false) . " ";
    echo $bootform->button("button", "Boton", "", "outline-info", false) . " ";
    echo $bootform->button("button", "Boton", "", "outline-warning", false) . " ";
    echo $bootform->button("button", "Boton", "", "outline-danger", false);
    echo $bootform->button("button", "Boton", "", "outline-dark", false);
    echo $bootform->button("button", "Boton", "", "outline-light", false);
    echo "<hr>";
    echo $bootform->button("button", "Boton", "sm", "primary") . " ";
    echo $bootform->button("button", "Boton", "sm", "secondary") . " ";
    echo $bootform->button("button", "Boton", "sm", "success") . " ";
    echo $bootform->button("button", "Boton", "sm", "info") . " ";
    echo $bootform->button("button", "Boton", "sm", "warning") . " ";
    echo $bootform->button("button", "Boton", "sm", "danger") . " ";
    echo $bootform->button("button", "Boton", "sm", "dark") . " ";
    echo $bootform->button("button", "Boton", "sm", "light") . " ";
    echo "<br>";
    echo $bootform->button("button", "Boton", "sm", "primary", false) . " ";
    echo $bootform->button("button", "Boton", "sm", "secondary", false) . " ";
    echo $bootform->button("button", "Boton", "sm", "success", false) . " ";
    echo $bootform->button("button", "Boton", "sm", "info", false) . " ";
    echo $bootform->button("button", "Boton", "sm", "warning", false) . " ";
    echo $bootform->button("button", "Boton", "sm", "danger", false);
    echo $bootform->button("button", "Boton", "sm", "dark", false);
    echo $bootform->button("button", "Boton", "sm", "light", false);
    echo "<hr>";
    echo $bootform->button("button", "Boton", "", "primary") . " ";
    echo $bootform->button("button", "Boton", "", "secondary") . " ";
    echo $bootform->button("button", "Boton", "", "success") . " ";
    echo $bootform->button("button", "Boton", "", "info") . " ";
    echo $bootform->button("button", "Boton", "", "warning") . " ";
    echo $bootform->button("button", "Boton", "", "danger") . " ";
    echo $bootform->button("button", "Boton", "", "dark") . " ";
    echo $bootform->button("button", "Boton", "", "light") . " ";
    echo "<br>";
    echo $bootform->button("button", "Boton", "", "primary", false) . " ";
    echo $bootform->button("button", "Boton", "", "secondary", false) . " ";
    echo $bootform->button("button", "Boton", "", "success", false) . " ";
    echo $bootform->button("button", "Boton", "", "info", false) . " ";
    echo $bootform->button("button", "Boton", "", "warning", false) . " ";
    echo $bootform->button("button", "Boton", "", "danger", false);
    echo $bootform->button("button", "Boton", "", "dark", false);
    echo $bootform->button("button", "Boton", "", "light", false);
    echo "<hr>";
    echo $bootform->button("button", "Boton", "lg", "primary") . " ";
    echo $bootform->button("button", "Boton", "lg", "secondary") . " ";
    echo $bootform->button("button", "Boton", "lg", "success") . " ";
    echo $bootform->button("button", "Boton", "lg", "info") . " ";
    echo $bootform->button("button", "Boton", "lg", "warning") . " ";
    echo $bootform->button("button", "Boton", "lg", "danger") . " ";
    echo $bootform->button("button", "Boton", "lg", "dark") . " ";
    echo $bootform->button("button", "Boton", "lg", "light") . " ";
    echo "<br>";
    echo $bootform->button("button", "Boton", "lg", "primary", false) . " ";
    echo $bootform->button("button", "Boton", "lg", "secondary", false) . " ";
    echo $bootform->button("button", "Boton", "lg", "success", false) . " ";
    echo $bootform->button("button", "Boton", "lg", "info", false) . " ";
    echo $bootform->button("button", "Boton", "lg", "warning", false) . " ";
    echo $bootform->button("button", "Boton", "lg", "danger", false);
    echo $bootform->button("button", "Boton", "lg", "dark", false);
    echo $bootform->button("button", "Boton", "lg", "light", false);
    echo "<hr>";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "sm", "primary") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "sm", "secondary") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "", "success") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "", "info") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "warning") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "danger") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "dark") . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "light") . " ";
    echo "<br>";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "sm", "primary", false) . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "sm", "secondary", false) . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "", "success", false) . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "", "info", false) . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "warning", false) . " ";
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "danger", false);
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "dark", false);
    echo $bootform->button("button", "<i class='far fa-envelope'></i>", "lg", "light", false);
    echo "<hr>";
    include("includes/foot.php");
?>